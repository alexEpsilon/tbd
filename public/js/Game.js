var app = new Vue({
    el: '#app',

    data: {
        joueur : [
            {},
            {
                pv: 100,
                mana:30
            },
            {
                pv: 100,
                mana:30
            },
            {
                pv: 100,
                mana:30
            },
            {
                pv: 100,
                mana:30
            }
        ],

        joueurQuiATaper : [],

        Monster : {
            pv: 800,
            mana: 100
        },

        capacite1J1 : "", capacite2J1 : "",  capacite3J1 : "",  capacite4J1 : "", capacite1J2 : "", capacite2J2 : "", capacite3J2 : "", capacite4J2 : "",capacite1J3 : "",capacite2J3 : "",capacite3J3 : "",capacite4J3 : "",capacite1J4 : "",capacite2J4 : "",capacite3J4 : "",capacite4J4 : "",

        attackSkill : [
             { 
                name : 'Poing',
                description: "",
                heal : 0,
                dmg : 1,
                mana : 0,
                gainMana : 2,
                buff : ""
            },
             { 
                name : "Pied",
                description: "",
                heal : 0,
                dmg : 100,
                mana : 0,
                gainMana : 0,
                buff : ""
            },
            { 
                name : "Dague",
                description: "",
                heal : 0,
                dmg : 7,
                mana : 0,
                gainMana : 10,
                buff : ""
            },
            { 
                name : "Fire",
                description: "",
                heal : 0,
                dmg : 6,
                gainMana : 0,
                mana : 10,
                buff : ""
            },
        ],

        defenseSkill : [
            {
                name : 'soin1',
                description: "",
                heal: 5,
                dmg: 0,  
                gainMana : 0,         
                mana : 3,
                buff : ""
                                                                
            },
            {
                name : 'soin2',
                description: "",
                heal: 7,
                dmg: 0,  
                gainMana : 0,         
                mana : 5,
                buff : ""
                                                                
            },
            {
                name : 'soin3',
                description: "",
                heal: 9,
                dmg: 0,    
                gainMana : 0,       
                mana : 10,
                buff : ""
                                                                
            },
            {
                name : 'soin4',
                description: "",
                heal: 11,
                dmg: 0, 
                gainMana : 0,          
                mana : 3,
                buff : ""
                                                                
            },
        ],
    },

    computed: {
      
    },

    methods: {
        degat: function(capacite, i){
            let clicked = document.getElementById("joueur" + i);
            
            if(this.Monster.pv > 0){
                
                if(this.joueurQuiATaper.includes(i)){
                    alert("ce joueur a déjà joué");                    
                }else{
                    this.Monster.pv -= capacite.dmg;
                    this.joueurQuiATaper.push(i);

                    this.joueur[i].pv += capacite.heal;
                    this.joueur[i].mana -= capacite.mana;
                    this.joueur[i].mana += capacite.gainMana;
                    setTimeout(() => { 
                        this.monsterDegat(i);
                    }, 500);
                    clicked.classList.add('selected');
                }
                if(this.joueurQuiATaper.length == 4){
                        document.getElementById("joueur" + this.joueurQuiATaper[0]).classList.remove('selected');                                        
                        document.getElementById("joueur" + this.joueurQuiATaper[1]).classList.remove('selected');                                        
                        document.getElementById("joueur" + this.joueurQuiATaper[2]).classList.remove('selected');                                        
                        document.getElementById("joueur" + this.joueurQuiATaper[3]).classList.remove('selected');                                        
                        this.joueurQuiATaper = [];
                        setTimeout(() => { 
                            this.monsterDegat(i);
                        }, 500);
                                                
                    } 
            }else {
                alert('il es mort !');
            }
            
        },
        monsterDegat: function(i) {
            let tapeQuelPerso = this.getRandomInt(3);
            if(tapeQuelPerso == 0){
                i = this.getRandomInt(3) + 1;
            }
            let degat = this.getRandomInt(20);
            this.joueur[i].pv -= degat;
            console.log(degat);
        },

        getRandomInt: function(max) {
            return Math.floor(Math.random() * Math.floor(max));
        },

        init: function(){
            this.capacite1J1 = this.attackSkill[this.getRandomInt(4)];
            this.capacite2J1 = this.attackSkill[this.getRandomInt(4)];
            this.capacite3J1 = this.attackSkill[this.getRandomInt(4)];
            this.capacite4J1 = this.defenseSkill[this.getRandomInt(4)];
        
            this.capacite1J2 = this.attackSkill[this.getRandomInt(4)];
            this.capacite2J2 = this.attackSkill[this.getRandomInt(4)];
            this.capacite3J2 = this.attackSkill[this.getRandomInt(4)];
            this.capacite4J2 = this.defenseSkill[this.getRandomInt(4)];
        
            this.capacite1J3 = this.attackSkill[this.getRandomInt(4)];
            this.capacite2J3 = this.attackSkill[this.getRandomInt(4)];
            this.capacite3J3 = this.attackSkill[this.getRandomInt(4)];
            this.capacite4J3 = this.defenseSkill[this.getRandomInt(4)];
        
            this.capacite1J4 = this.attackSkill[this.getRandomInt(4)];
            this.capacite2J4 = this.attackSkill[this.getRandomInt(4)];
            this.capacite3J4 = this.attackSkill[this.getRandomInt(4)];
            this.capacite4J4 = this.defenseSkill[this.getRandomInt(4)];
        }
        

    },

    beforeMount(){
        this.init()
    },


})
