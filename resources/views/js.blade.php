

<script src="https://unpkg.com/vue@latest/dist/vue.js"></script>

<script>

var app = new Vue({
    el: '#app',

    data: {
        //stats des joueurs

        joueur : [
            //premier objet vide : j1 == joueur[1]
            {}, {pv: 100,pvMax: 100, mana:30, manaMax: 30}, {pv: 100,pvMax: 100, mana:30, manaMax: 30},{pv: 100,pvMax: 100, mana:30, manaMax: 30},{pv: 100,pvMax: 100, mana:30, manaMax: 30},
        ],

        joueurMort : [],        
        joueurQuiATaper : [],

        //stats du monstre                    
        Monster : { pv: 800, pvMax: 800, mana: 100, manaMax: 100},
        degatMonster : "",

        capacite1J1 : "", capacite2J1 : "",  capacite3J1 : "",  capacite4J1 : "", capacite1J2 : "", capacite2J2 : "", capacite3J2 : "", capacite4J2 : "",capacite1J3 : "",capacite2J3 : "",capacite3J3 : "",capacite4J3 : "",capacite1J4 : "",capacite2J4 : "",capacite3J4 : "",capacite4J4 : "",
        capacite1Monstre: "",
        capacite2Monstre: "",
        capacite3Monstre: "",
        capacite4Monstre: "",
//liste des capacité offensive
        attackSkill : [
             { 
                name : 'Poing',
                description: "",
                heal : 0,
                dmg : 1,
                mana : 1,
                gainMana : 0,
                buff : ""
            },
             { 
                name : "Pied",
                description: "",
                heal : 0,
                dmg : 5,
                mana : 5,
                gainMana : 0,
                buff : ""
            },
            { 
                name : "Dague",
                description: "",
                heal : 0,
                dmg : 7,
                mana : 10,
                gainMana : 0,
                buff : ""
            },
            { 
                name : "Fire",
                description: "",
                heal : 0,
                dmg : 6,
                gainMana : 0,
                mana : 10,
                buff : ""
            },
        ],
//liste des capacité deffensive
        defenseSkill : [
            {
                name : 'soin1',
                description: "",
                heal: 5,
                dmg: 0,  
                gainMana : 0,         
                mana : 3,
                buff : ""
                                                                
            },
            {
                name : 'soin2',
                description: "",
                heal: 7,
                dmg: 0,  
                gainMana : 0,         
                mana : 5,
                buff : ""
                                                                
            },
            {
                name : 'soin3',
                description: "",
                heal: 9,
                dmg: 0,    
                gainMana : 0,       
                mana : 10,
                buff : ""
                                                                
            },
            {
                name : 'soin4',
                description: "",
                heal: 11,
                dmg: 0, 
                gainMana : 0,          
                mana : 3,
                buff : ""
                                                                
            },
        ],
//liste des capacité régenerante
        manaSkill : [
            {
                name : 'regen',
                description: "",
                heal: 0,
                dmg: 0, 
                gainMana : 10,          
                mana : 0,
                buff : ""
            },
            {
                name : 'regen 2',
                description: "",
                heal: 0,
                dmg: 0, 
                gainMana : 15,          
                mana : 0,
                buff : ""
            },
            {
                name : 'regen 3',
                description: "",
                heal: -5,
                dmg: 0, 
                gainMana : 20,          
                mana : 0,
                buff : ""
            },
            {
                name : 'regen 4',
                description: "",
                heal: -10,
                dmg: 0,
                gainMana : 25,
                mana : 0,
                buff : ""
            },
        ],
//fin capacité
    },

    computed: {
       
    },

    methods: {
        styleWidthProgessBar(pv, pvMax) {
            let pourcentage = (pv * 100) / pvMax;
            return "width:" + pourcentage + "%";
        },
//attaque du joueur
        degat: function(capacite, i){
            //récupération du donneur de coup
            let clicked = document.getElementById("joueur" + i);
            //verification si le joueur est mort
            if(!this.joueurMort.includes(i)){
                if(this.Monster.pv > 0){
                
                    //verification si le joueur a jouer.
                    if(this.joueurQuiATaper.includes(i)){
                        alert("ce joueur a déjà joué");                    
                    }else{
                        //décrémentation des pv du monstre
                        this.Monster.pv -= capacite.dmg;
                        //le donneur de coup a jouer
                        this.joueurQuiATaper.push(i);
                        //gestion des modificateur de status
                        this.joueur[i].pv += capacite.heal;
                        this.joueur[i].mana -= capacite.mana;
                        this.joueur[i].mana += capacite.gainMana;

                        //animation des degats sur le monstre
                        let animationDesDegatSurMonstre = document.getElementById("monsterCard");
                        let affichageDesDegatSurMonstre = document.getElementById("degatSpanMonster");
                        let animationHealingSurJoueur = document.getElementById("joueur" + i);

                        if(capacite.heal > 0){
                            animationHealingSurJoueur.classList.add("AnimationHeal");
                            setTimeout(() => { 
                                animationHealingSurJoueur.classList.remove('AnimationHeal');
                            }, 300)
                        }
                        
                        if(capacite.dmg > 0){
                            animationDesDegatSurMonstre.classList.add('animationDegatsCard');
                            affichageDesDegatSurMonstre.classList.add('animationAffichageDesDegats');

                            affichageDesDegatSurMonstre.innerHTML = capacite.dmg;

                            setTimeout(() => { 
                            affichageDesDegatSurMonstre.classList.remove('animationAffichageDesDegats');
                            animationDesDegatSurMonstre.classList.remove('animationDegatsCard');
                            affichageDesDegatSurMonstre.innerHTML = "";
                            }, 1000)

                            //lance la riposte du monstre
                            setTimeout(() => { 
                                this.monsterDegat(i, true);
                            }, 500);
                        }
                        
                        clicked.classList.add('selected');
                    }

                    //réinitialisation des joueurs
                    if(this.joueurQuiATaper.length == 4){
                            document.getElementById("joueur" + this.joueurQuiATaper[0]).classList.remove('selected');                                        
                            document.getElementById("joueur" + this.joueurQuiATaper[1]).classList.remove('selected');                                        
                            document.getElementById("joueur" + this.joueurQuiATaper[2]).classList.remove('selected');                                        
                            document.getElementById("joueur" + this.joueurQuiATaper[3]).classList.remove('selected');                                        
                            this.joueurQuiATaper = [];
                    //le monstre riposte au reset                        
                            setTimeout(() => { 
                                this.monsterDegat(i, false);
                            }, 500);
                                                    
                        }
                
                }else {
                    //code si monstre mort
                    alert('il es mort !');
                }
            }else { alert('joueur mort !')}
        },

//attaque du monstre
        monsterDegat: function(i, randomMode) {
            
            let attack = true; 

            if(randomMode == true){
                let chance = this.getRandomInt(3);

                if(chance !== 2){
                    attack = false
                }
            }
            
            if(attack == true){
                //ciblage avec part d'aléatoire
            let tapeQuelPerso = this.getRandomInt(3);
                        
                if(tapeQuelPerso == 0){
                    i = this.getRandomInt(3) + 1;
                }

                //dégats du monstre                                            
                this.degatMonster = this.getRandomInt(20);
                //décrementation des pv du joueur
                this.joueur[i].pv -= this.degatMonster;
                //verification mort du joueur
                if(this.joueur[i].pv <= 0){
                    this.joueurMort.push(i);
                    alert('J'+i+' est mort');
                }
    
                
                //animation des dégats
                let animationDesDegatsSurJoueur = document.getElementById("degatSpanJ" + i);
                let animationDesDegatsSurJoueurShake = document.getElementById("joueur" + i);
    
                animationDesDegatsSurJoueur.innerHTML = this.degatMonster;
                animationDesDegatsSurJoueur.classList.add('animationAffichageDesDegats');
                if(this.degatMonster !== 0){
                    animationDesDegatsSurJoueurShake.classList.add('animationDegatsCard');
                }else{
                    animationDesDegatsSurJoueur.innerHTML = "FAIL !";  
                }
                setTimeout(() => { 
                    animationDesDegatsSurJoueur.classList.remove('animationAffichageDesDegats');
                    animationDesDegatsSurJoueur.innerHTML = "";
                    animationDesDegatsSurJoueurShake.classList.remove('animationDegatsCard');
                }, 700);
            
            }
            
        },
//custom random integer
        getRandomInt: function(max) {
            return Math.floor(Math.random() * Math.floor(max));
        },


// initialise les capacités aléatoirement pour chaques joueurs
        init: function(){
            this.capacite1J1 = this.attackSkill[this.getRandomInt(this.attackSkill.length)];
            this.capacite2J1 = this.attackSkill[this.getRandomInt(this.attackSkill.length)];
            this.capacite3J1 = this.manaSkill[this.getRandomInt(this.manaSkill.length)];
            this.capacite4J1 = this.defenseSkill[this.getRandomInt(this.defenseSkill.length)];
        
            this.capacite1J2 = this.attackSkill[this.getRandomInt(this.attackSkill.length)];
            this.capacite2J2 = this.attackSkill[this.getRandomInt(this.attackSkill.length)];
            this.capacite3J2 = this.manaSkill[this.getRandomInt(this.manaSkill.length)];
            this.capacite4J2 = this.defenseSkill[this.getRandomInt(this.defenseSkill.length)];
        
            this.capacite1J3 = this.attackSkill[this.getRandomInt(this.attackSkill.length)];
            this.capacite2J3 = this.attackSkill[this.getRandomInt(this.attackSkill.length)];
            this.capacite3J3 = this.manaSkill[this.getRandomInt(this.manaSkill.length)];
            this.capacite4J3 = this.defenseSkill[this.getRandomInt(this.defenseSkill.length)];
        
            this.capacite1J4 = this.attackSkill[this.getRandomInt(this.attackSkill.length)];
            this.capacite2J4 = this.attackSkill[this.getRandomInt(this.attackSkill.length)];
            this.capacite3J4 = this.manaSkill[this.getRandomInt(this.manaSkill.length)];
            this.capacite4J4 = this.defenseSkill[this.getRandomInt(this.defenseSkill.length)];
        }
        

    },
//init values
    beforeMount(){
        this.init()
    },


})


</script>


