@extends('layout.app')
@section('content')
<div id="app">
    <section class="container-fluid">
        <div class="row">
            <div class="col-sm-3 card" id="joueur1">
                <div class="card-body">
                    <h5 class="card-title">J1</h5>
                    <p class="col-12"><i class="fas fa-heart"></i> : <span id='J1'>@{{joueur[1].pv}}</span></p>
                    <div class="progress md-progress">
                        <div class="progress-bar bg-danger" role="progressbar" :style="styleWidthProgessBar(joueur[1].pv, joueur[1].pvMax)" :aria-valuenow="joueur[1].pv" aria-valuemin="0" :aria-valuemax="joueur[1].pvMax"></div>   
                    </div>
                    <p class="col-12"><i class="fas fa-fire-alt"></i> : <span id='manaJ1'>@{{joueur[1].mana}}</span></p>
                    <div class="progress md-progress">
                        <div class="progress-bar" role="progressbar" :style="styleWidthProgessBar(joueur[1].mana, joueur[1].manaMax)" :aria-valuenow="joueur[1].mana" aria-valuemin="0" :aria-valuemax="joueur[1].manaMax"></div>   
                    </div>
                    <span class="badge badge-danger ml-2 " id="degatSpanJ1"></span>
                        <div class="row">

                            <button v-if="capacite1J1.mana <= joueur[1].mana" 
                                    type="button" 
                                    v-on:click="degat(capacite1J1, 1)" 
                                    class="btn btn-success material-tooltip-main" 
                                    >
                                        @{{capacite1J1.name}}  
                                        <i class="fas fa-bomb"></i> @{{capacite1J1.dmg}} 
                                        <i class="fas fa-fire-alt"></i> - @{{capacite1J1.mana}}
                            </button>

                            <button 
                                v-else 
                                type="button" 
                                class="btn btn-primary"
                                >
                                    @{{capacite1J1.name}}
                                    <i class="fas fa-fire-alt"></i> - @{{capacite1J1.mana}}
                            </button>

                            <button 
                                v-if="capacite2J1.mana <= joueur[1].mana" 
                                type="button"
                                v-on:click="degat(capacite2J1, 1)" 
                                class="btn btn-success"
                                >
                                    @{{capacite2J1.name}} <i class="fas fa-bomb"></i> @{{capacite2J1.dmg}} <i class="fas fa-fire-alt"></i> - @{{capacite2J1.mana}}
                            </button>

                            <button 
                                v-else 
                                type="button" 
                                class="btn btn-primary"
                                >
                                    @{{capacite2J1.name}}
                                    <i class="fas fa-fire-alt"></i> - @{{capacite2J1.mana}}
                            </button>

                            <button 
                                v-if="capacite3J1.mana <= joueur[1].mana" 
                                type="button"
                                v-on:click="degat(capacite3J1, 1)" 
                                class="btn btn-success"
                                >
                                    @{{capacite3J1.name}} <i class="fas fa-heart"></i> @{{capacite3J1.heal}} <i class="fas fa-fire-alt"></i> + @{{capacite3J1.gainMana}}
                            </button>

                            <button 
                                v-else 
                                type="button" 
                                class="btn btn-primary"
                                >
                                    @{{capacite3J1.name}}
                                    <i class="fas fa-fire-alt"></i> - @{{capacite3J1.mana}}
                            </button>

                            <button 
                                v-if="capacite4J1.mana <= joueur[1].mana" 
                                type="button"
                                v-on:click="degat(capacite4J1, 1)" 
                                class="btn btn-success"
                                >
                                    @{{capacite4J1.name}} <i class="fas fa-heart"></i> @{{capacite4J1.heal}} <i class="fas fa-fire-alt"></i> - @{{capacite4J1.mana}}
                            </button>

                            <button 
                                v-else 
                                type="button" 
                                class="btn btn-primary"
                                >
                                    @{{capacite4J1.name}}
                                    <i class="fas fa-fire-alt"></i> - @{{capacite4J1.mana}}
                            </button>


                        </div>
                    </div>
                </div>
            
            <div class="col-sm-3 card " id="joueur2">
                <div class="card-body">
                    <h5 class="card-title">J2</h5>
                    <p class="col-12"><i class="fas fa-heart"></i> : <span id='J2'>@{{ joueur[2].pv }}</span></p>
                    <div class="progress md-progress">
                        <div class="progress-bar bg-danger" role="progressbar" :style="styleWidthProgessBar(joueur[2].pv, joueur[2].pvMax)" :aria-valuenow="joueur[2].pv" aria-valuemin="0" :aria-valuemax="joueur[2].pvMax"></div>   
                    </div>
                    <p class="col-12"><i class="fas fa-fire-alt"></i> : <span id='manaJ2'>@{{ joueur[2].mana }}</span></p>
                    <div class="progress md-progress">
                        <div class="progress-bar" role="progressbar" :style="styleWidthProgessBar(joueur[2].mana, joueur[2].manaMax)" :aria-valuenow="joueur[2].mana" aria-valuemin="0" :aria-valuemax="joueur[2].manaMax"></div>   
                    </div>
                    <span class="badge badge-danger ml-2" id="degatSpanJ2"></span>
                    <div class="row">
                
                        <button 
                            v-if="capacite1J2.mana <= joueur[2].mana" 
                            type="button"
                            v-on:click="degat(capacite1J2, 2)" 
                            class="btn btn-success"
                            >
                                @{{capacite1J2.name}}<i class="fas fa-bomb"></i> @{{capacite1J2.dmg}}<i class="fas fa-fire-alt"></i> - @{{capacite1J2.mana}}
                        </button>

                        <button 
                            v-else 
                            type="button" 
                            class="btn btn-primary"
                            >
                                @{{capacite1J2.name}}
                                <i class="fas fa-fire-alt"></i> - @{{capacite1J2.mana}}
                        </button>

                        <button 
                            v-if="capacite2J2.mana <= joueur[2].mana" 
                            type="button"
                            v-on:click="degat(capacite2J2, 2)" 
                            class="btn btn-success"
                            >
                                @{{capacite2J2.name}} <i class="fas fa-bomb"></i> @{{capacite2J2.dmg}} <i class="fas fa-fire-alt"></i> - @{{capacite2J2.mana}}
                        </button>

                        <button 
                            v-else 
                            type="button" 
                            class="btn btn-primary"
                            >
                                @{{capacite2J2.name}}
                                <i class="fas fa-fire-alt"></i> - @{{capacite2J2.mana}}
                        </button>

                        <button 
                            v-if="capacite3J2.mana <= joueur[2].mana" 
                            type="button"
                            v-on:click="degat(capacite3J2, 2)" 
                            class="btn btn-success"
                            >
                                @{{capacite3J2.name}} <i class="fas fa-heart"></i> @{{capacite3J2.heal}} <i class="fas fa-fire-alt"></i> + @{{capacite3J2.gainMana}}
                        </button>
                        
                        <button 
                            v-else 
                            type="button" 
                            class="btn btn-primary"
                            >
                                @{{capacite3J2.name}}
                                <i class="fas fa-fire-alt"></i> - @{{capacite3J2.mana}}
                        </button>
                        
                        <button 
                            v-if="capacite4J2.mana <= joueur[2].mana" 
                            type="button"
                            v-on:click="degat(capacite4J2, 2)" 
                            class="btn btn-success"
                            >
                                @{{capacite4J2.name}} <i class="fas fa-heart"></i> @{{capacite4J2.heal}}<i class="fas fa-fire-alt"></i> - @{{capacite4J2.mana}}
                        </button>
                        
                        <button 
                            v-else 
                            type="button" 
                            class="btn btn-primary"
                            >
                                @{{capacite4J2.name}}
                                <i class="fas fa-fire-alt"></i> - @{{capacite4J2.mana}}
                        </button>


                    </div>
                </div>
            </div>

            <div class="col-sm-3 card " id="joueur3">
                <div class="card-body">
                    <h5 class="card-title">J3</h5>
                    <p class="col-12"><i class="fas fa-heart"></i> : <span id='J3'>@{{ joueur[3].pv }}</span></p>
                    <div class="progress md-progress">
                        <div class="progress-bar bg-danger" role="progressbar" :style="styleWidthProgessBar(joueur[3].pv, joueur[3].pvMax)" :aria-valuenow="joueur[3].pv" aria-valuemin="0" :aria-valuemax="joueur[3].pvMax"></div>   
                    </div>
                    <p class="col-12"><i class="fas fa-fire-alt"></i> : <span id='manaJ3'>@{{ joueur[3].mana }}</span></p>
                    <div class="progress md-progress">
                        <div class="progress-bar" role="progressbar" :style="styleWidthProgessBar(joueur[3].mana, joueur[3].manaMax)" :aria-valuenow="joueur[3].mana" aria-valuemin="0" :aria-valuemax="joueur[3].manaMax"></div>   
                    </div>
                    <span class="badge badge-danger ml-2" id="degatSpanJ3"></span>
                    <div class="row">

                        <button 
                            v-if="capacite1J3.mana <= joueur[3].mana "  
                            type="button"
                            v-on:click="degat(capacite1J3, 3)" 
                            class="btn btn-success"
                            >
                                @{{capacite1J3.name}} <i class="fas fa-bomb"></i> @{{capacite1J3.dmg}} <i class="fas fa-fire-alt"></i> - @{{capacite1J3.mana}}
                        </button>

                        <button 
                            v-else 
                            type="button" 
                            class="btn btn-primary"
                            >
                                @{{capacite1J3.name}}
                                <i class="fas fa-fire-alt"></i> - @{{capacite1J3.mana}}
                        </button>

                        <button 
                            v-if="capacite2J3.mana <= joueur[3].mana "  
                            type="button"
                            v-on:click="degat(capacite2J3, 3)" 
                            class="btn btn-success"
                            >
                                @{{capacite2J3.name}} <i class="fas fa-bomb"></i> @{{capacite2J3.dmg}} <i class="fas fa-fire-alt"></i> - @{{capacite2J3.mana}}
                        </button>

                        <button 
                            v-else 
                            type="button" 
                            class="btn btn-primary"
                            >
                                @{{capacite2J3.name}}
                                <i class="fas fa-fire-alt"></i> - @{{capacite2J3.mana}}
                        </button>

                        <button 
                            v-if="capacite3J3.mana <= joueur[3].mana "  
                            type="button"
                            v-on:click="degat(capacite3J3, 3)" 
                            class="btn btn-success"
                            >
                                @{{capacite3J3.name}} <i class="fas fa-heart"></i> @{{capacite3J3.heal}} <i class="fas fa-fire-alt"></i> + @{{capacite3J3.gainMana}}
                        </button>

                        <button 
                            v-else 
                            type="button" 
                            class="btn btn-primary"
                            >
                                @{{capacite3J3.name}}
                                <i class="fas fa-fire-alt"></i> - @{{capacite3J3.mana}}
                        </button>

                        <button 
                            v-if="capacite4J3.mana <= joueur[3].mana "  
                            type="button"
                            v-on:click="degat(capacite4J3, 3)" 
                            class="btn btn-success"
                            >
                                @{{capacite4J3.name}} <i class="fas fa-heart"></i> @{{capacite4J3.heal}} <i class="fas fa-fire-alt"></i> - @{{capacite4J3.mana}}
                        </button>

                        <button 
                            v-else 
                            type="button" 
                            class="btn btn-primary"
                            >
                                @{{capacite4J3.name}}
                                <i class="fas fa-fire-alt"></i> - @{{capacite4J3.mana}}
                        </button>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-3 card center" id="joueur4">
                <div class="card-body text-center">
                    <h5 class="card-title">J4</h5>
                    <p class="col-12"><i class="fas fa-heart"></i> : <span id='J4'>@{{ joueur[4].pv }}</span></p>
                    <div class="progress md-progress">
                        <div class="progress-bar bg-danger" role="progressbar" :style="styleWidthProgessBar(joueur[4].pv, joueur[4].pvMax)" :aria-valuenow="joueur[4].pv" aria-valuemin="0" :aria-valuemax="joueur[4].pvMax"></div>   
                    </div>
                    <p class="col-12"><i class="fas fa-fire-alt"></i> : <span id='manaJ4'>@{{ joueur[4].mana }}</span></p>
                    <div class="progress md-progress">
                        <div class="progress-bar" role="progressbar" :style="styleWidthProgessBar(joueur[4].mana, joueur[4].manaMax)" :aria-valuenow="joueur[4].mana" aria-valuemin="0" :aria-valuemax="joueur[4].manaMax"></div>   
                    </div>
                    <div class="row text-right">
                        <button 
                            v-if="capacite1J4.mana <= joueur[4].mana " 
                            type="button"
                            v-on:click="degat(capacite1J4, 4)" class="btn btn-success"
                            >
                                @{{capacite1J4.name}} <i class="fas fa-bomb"></i> @{{capacite1J4.dmg}} <i class="fas fa-fire-alt"></i> - @{{capacite1J4.mana}}
                        </button>
                        <button 
                            v-else 
                            type="button" 
                            class="btn btn-primary"
                            >
                                @{{capacite1J4.name}}
                                <i class="fas fa-fire-alt"></i> - @{{capacite1J4.mana}}
                        </button>
                        <button 
                            v-if="capacite2J4.mana <= joueur[4].mana " 
                            type="button"
                            v-on:click="degat(capacite2J4, 4)" class="btn btn-success"
                            >
                                @{{capacite2J4.name}} <i class="fas fa-bomb"></i> @{{capacite2J4.dmg}} <i class="fas fa-fire-alt"></i> - @{{capacite2J4.mana}}
                        </button>
                        <button 
                            v-else 
                            type="button" 
                            class="btn btn-primary"
                            >
                                @{{capacite2J4.name}}
                                <i class="fas fa-fire-alt"></i> - @{{capacite2J4.mana}}
                        </button>
                        <span class="badge badge-danger text-right ml-2 justify-content-center" id="degatSpanJ4"></span>
                        <button 
                            v-if="capacite3J4.mana <= joueur[4].mana " 
                            type="button"
                            v-on:click="degat(capacite3J4, 4)" class="btn btn-success"
                            >
                                @{{capacite3J4.name}} <i class="fas fa-heart"></i> @{{capacite3J4.heal}} <i class="fas fa-fire-alt"></i> + @{{capacite3J4.gainMana}}
                        </button>
                        <button 
                            v-else 
                            type="button"
                            class="btn btn-primary"
                            >
                                @{{capacite3J4.name}}
                                <i class="fas fa-fire-alt"></i> - @{{capacite3J4.mana}}
                        </button>
                        <button 
                            v-if="capacite4J4.mana <= joueur[4].mana " 
                            type="button"
                            v-on:click="degat(capacite4J4, 4)" class="btn btn-success"
                            >
                                @{{capacite4J4.name}} <i class="fas fa-heart"></i> @{{capacite4J4.heal}} <i class="fas fa-fire-alt"></i> - @{{capacite4J4.mana}}
                        </button>
                        <button 
                            v-else 
                            type="button"
                            class="btn btn-primary"
                            >   
                                @{{capacite4J4.name}}
                                <i class="fas fa-fire-alt"></i> - @{{capacite4J4.mana}}
                        </button>
                    </div>
                    
                </div>
            </div>
        </div>
    </section>
    <hr>
    <section>
        <div class="container">
            <div class="card " id="monsterCard">
                <div class="card-body">
                    <h2>Le Monstre</h2>
                    <div class="progress md-progress">
                        <div class="progress-bar bg-danger" role="progressbar" :style="styleWidthProgessBar( Monster.pv, Monster.pvMax)" :aria-valuenow="Monster.pv" aria-valuemin="0" :aria-valuemax="Monster.pvMax"></div>   
                    </div>
                <p><i class="fas fa-heart"></i> : <span id="monster">@{{ Monster.pv }}</span></p>
                <span class="badge badge-danger ml-2 " id="degatSpanMonster"></span>
                <p class="col-12"><i class="fas fa-fire-alt"></i> : <span id='manaJ4manamonster'>@{{ Monster.mana }}</span></p>
                <div class="progress md-progress">
                    <div class="progress-bar" role="progressbar" :style="styleWidthProgessBar(Monster.mana, Monster.manaMax)" :aria-valuenow="Monster.mana" aria-valuemin="0" :aria-valuemax="Monster.manaMax"></div>   
                </div>
                </div>
            </div>
        </div>
    </section>
</div>
@include('js')
@endsection
